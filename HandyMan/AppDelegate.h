//
//  AppDelegate.h
//  HandyMan
//
//  Created by shustreek on 25.10.16.
//  Copyright (c) 2016 shustreek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

