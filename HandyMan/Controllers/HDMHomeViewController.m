//
//  HDMHomeViewController.m
//  HandyMan
//
//  Created by shustreek on 25.10.16.
//  Copyright (c) 2016 shustreek. All rights reserved.
//

#import "HDMHomeViewController.h"
#import "HDMCommunicateCell.h"
#import "HDMReviewCell.h"

@interface HDMHomeViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation HDMHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UINib *nib = [UINib nibWithNibName:@"HDMCommunicateCell" bundle:nil];
    [[self tableView] registerNib:nib forCellReuseIdentifier:@"HDMCommunicateCell"];
    UINib *nib2 = [UINib nibWithNibName:@"HDMReviewCell" bundle:nil];
    [[self tableView] registerNib:nib2 forCellReuseIdentifier:@"HDMReviewCell"];
    self.tableView.estimatedRowHeight = 150.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}
#pragma mark - Actions

- (IBAction)segmentSwitch:(id)sender {
    UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
    NSInteger selectedSegment = segmentedControl.selectedSegmentIndex;
    
    if (selectedSegment == 0) {
        
    }
    else{
        
    }
}
# pragma mark - Table View Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)theTableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)theTableView numberOfRowsInSection:(NSInteger)section
{
    return 2; // number of xibs which we use for Table View (must be 6 in the final version)
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath // different xibs for different part of design
{
    
    if (indexPath.row == 0) {
     HDMCommunicateCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HDMCommunicateCell"];
        return cell;
    } else {
     HDMReviewCell *cell2 = [tableView dequeueReusableCellWithIdentifier:@"HDMReviewCell"];
        return cell2;
    }
}

@end
